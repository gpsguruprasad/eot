package com.bootcamp;

import java.util.Scanner;

public class Starter {
    public static void main(String ...args){
        GameConsole gameConsole = new GameConsole(new Scanner(System.in));
        Game game = new Game(new CooperatePlayer("Player1"), new CheatPlayer("Player2"), new Machine(), gameConsole);
        game.play();
    }
}
