package com.bootcamp;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Game {

    Player player1;
    Player player2;
    Machine machine;
    Moves previousMoves;
    private PropertyChangeSupport support;

    private GameConsole gameConsole;

    public Game(Player player1, Player player2, Machine machine, GameConsole scanner){
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.gameConsole = scanner;
        support = new PropertyChangeSupport(this);
    }

    public void addPropertyChangeListener(PropertyChangeListener pcl){
        support.addPropertyChangeListener(pcl);
    }

    public void play() {
        Moves currentMoves;
        gameConsole.printToConsole("Input number of moves");
        int moves = Integer.parseInt(gameConsole.nextLine());
        for(int i = 0; i< moves; i++) {
            String playerOneChoice = player1.makeAMove();
            String playerTwoChoice = player2.makeAMove();
            publishMoves(playerOneChoice, playerTwoChoice);
            Score score = machine.input(Move.getEnumForValue(playerOneChoice), Move.getEnumForValue(playerTwoChoice));
            player1.updateScore(score.getScore1());
            player2.updateScore(score.getScore2());
            gameConsole.printToConsole(showScore());
        }
    }

    private void publishMoves(String playerOneChoice, String playerTwoChoice) {
        Moves currentMoves;
        currentMoves = new Moves();
        currentMoves.move1 = playerOneChoice;
        currentMoves.move2 = playerTwoChoice;
        support.firePropertyChange("moves", this.previousMoves, currentMoves);
        this.previousMoves = currentMoves;
    }

    public String showScore() {
        String message = this.player1.getName() + " points : " + this.player1.getScore() + ", " + this.player2.getName() + " points : " + this.player2.getScore();
        return message;
    }
}
