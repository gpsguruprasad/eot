package com.bootcamp;

public class CheatPlayer extends Player {
    public CheatPlayer(String name) {
        super(name);
    }

    public String makeAMove() {
        return Move.Cheat.name;
    }
}
