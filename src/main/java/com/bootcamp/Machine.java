package com.bootcamp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Machine {

    private static Map<String , Score> scoreMap = new HashMap<>();

    {
        scoreMap.put(Move.Cheat + "|" + Move.Cheat, new Score(0,0));
        scoreMap.put(Move.Cooperate+ "|" + Move.Cooperate, new Score(2,2));
        scoreMap.put(Move.Cheat + "|" + Move.Cooperate, new Score(3,-1));
        scoreMap.put(Move.Cooperate+ "|" + Move.Cheat, new Score(-1,3));
    }

    public Score input(Move input1, Move input2){
        return scoreMap.get(input1 + "|" + input2);
    }
}
