package com.bootcamp;

import java.util.Scanner;

public class GameConsole {

    private Scanner scanner;

    public GameConsole(Scanner scanner) {
        this.scanner = scanner;
    }

    public String nextLine() {
        return scanner.nextLine();
    }

    public void printToConsole(String text){
        System.out.println(text);
    }
}