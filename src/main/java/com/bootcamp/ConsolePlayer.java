package com.bootcamp;

public class ConsolePlayer extends Player {

    private GameConsole gameConsole;

    public ConsolePlayer(String name, GameConsole gameConsole){
        super(name);
        this.gameConsole = gameConsole;
    }

    public String makeAMove() {
        return gameConsole.nextLine();
    }
}
