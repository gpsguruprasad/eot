package com.bootcamp;

public abstract class Player {
    protected String name;
    private int score;

    public Player(String name) {
        this.name = name;
    }

    public void updateScore(int i) {
        score += i;
    }

    public int getScore() {
        return score;
    }

    public String getName() {
        return name;
    }

    public abstract String makeAMove();
}
