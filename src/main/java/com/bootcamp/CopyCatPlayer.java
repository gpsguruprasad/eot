package com.bootcamp;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class CopyCatPlayer extends Player implements PropertyChangeListener {

    Moves currentMovesFromGame;
    Move nextMove;

    public CopyCatPlayer(String name) {
        super(name);
        nextMove = Move.Cooperate;
    }

    @Override
    public String makeAMove() {
        return nextMove.name;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.currentMovesFromGame = (Moves) evt.getNewValue();

        if(this.currentMovesFromGame.move1.equals(nextMove.name)) {
            nextMove = Move.getEnumForValue(this.currentMovesFromGame.move2);
        }
        else   {
            nextMove = Move.getEnumForValue(this.currentMovesFromGame.move1);
        }
    }
}
