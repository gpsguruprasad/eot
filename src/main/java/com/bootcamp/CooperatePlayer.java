package com.bootcamp;

public class CooperatePlayer extends Player {
    public CooperatePlayer(String name) {
        super(name);
    }

    public String makeAMove() {
        return Move.Cooperate.name;
    }
}