package com.bootcamp;

public enum Move {
    Cooperate("co"),
    Cheat("ch");

    String name;

    Move(String name){
        this.name = name;
    }

    public static Move getEnumForValue(String val){
        Move[] moveList = Move.values();
        for(int i =0 ; i < moveList.length; i++){
            if(moveList[i].name.equals(val)){
                return moveList[i];
            }
        }
        return null;
    }
}
