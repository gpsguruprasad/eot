package com.bootcamp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;

public class ConsolePlayerTest {

    private GameConsole gameConsoleMock;

    @Before
    public void setup(){
        this.gameConsoleMock = Mockito.mock(GameConsole.class);
    }

    @Test
    public void score_updated_to_2(){
        ConsolePlayer consolePlayer = new ConsolePlayer("player1", gameConsoleMock);
        consolePlayer.updateScore(2);
        assertEquals(2, consolePlayer.getScore());
    }

    @Test
    public void should_make_cheat_move_from_console(){
        Mockito.when(gameConsoleMock.nextLine())
                .thenReturn("ch");
        Player player = new ConsolePlayer("player1", gameConsoleMock);
        assertEquals("ch", player.makeAMove());
    }

    @Test
    public void should_make_cooperate_move_from_console(){
        Mockito.when(gameConsoleMock.nextLine())
                .thenReturn("co");
        Player player = new ConsolePlayer("player1", gameConsoleMock);
        assertEquals("co", player.makeAMove());
    }
}
