package com.bootcamp;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CooperatePlayerTest {

    @Test
    public void should_always_cooperate(){
        Player player = new CooperatePlayer("player1");
        assertEquals("co", player.makeAMove());
    }
}
