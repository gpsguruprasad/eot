package com.bootcamp;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class MachineTest {
    @Test
    public void should_return_2x2_for_co_co() {
        Machine machine = new Machine();
        Score score = machine.input(Move.Cooperate, Move.Cooperate);
        assertEquals(2, score.getScore1());
        assertEquals(2, score.getScore2());
    }

    @Test
    public void should_return_0_and_0_for_ch_ch() {
        Machine machine = new Machine();
        Score score = machine.input(Move.Cheat,Move.Cheat);
        assertEquals(0, score.getScore1());
        assertEquals(0, score.getScore2());
    }

    @Test
    public void should_return_0_and_0_for_co_ch() {
        Machine machine = new Machine();
        Score score = machine.input(Move.Cooperate,Move.Cheat);
        assertEquals(-1, score.getScore1());
        assertEquals(3, score.getScore2());
    }

    @Test
    public void should_return_3_and_negative_1_for_ch_co() {
        Machine machine = new Machine();
        Score score = machine.input(Move.Cheat,Move.Cooperate);
        assertEquals(3, score.getScore1());
        assertEquals(-1, score.getScore2());
    }
}
