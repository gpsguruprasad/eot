package com.bootcamp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;

public class CopyCatPlayerTest {

    private GameConsole gameConsoleMock;

    @Before
    public void setup(){
        this.gameConsoleMock = Mockito.mock(GameConsole.class);
    }

    @Test
    public void should_always_send_cooperate_as_first_move(){
        Player player = new CopyCatPlayer("player1");
        assertEquals("co", player.makeAMove());
    }
}
