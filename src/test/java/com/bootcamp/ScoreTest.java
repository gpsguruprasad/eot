package com.bootcamp;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ScoreTest {
    @Test
    public void should_get_score1() {
        Score score = new Score(1,0);
        assertEquals(1, score.getScore1());
    }

    @Test
    public void should_get_score2() {
        Score score = new Score(0,2);
        assertEquals(2, score.getScore2());
    }
}
