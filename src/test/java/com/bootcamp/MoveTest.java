package com.bootcamp;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MoveTest {
    @Test
    public void getEnumForValue() {
        assertEquals(Move.Cooperate, Move.getEnumForValue("co"));
        assertEquals(Move.Cheat, Move.getEnumForValue("ch"));
    }
}
