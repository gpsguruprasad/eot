package com.bootcamp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;

public class GameTest {

    static Game game;
    private GameConsole gameConsoleMock;

    @Before
    public void _setup()
    {
        this.gameConsoleMock = Mockito.mock(GameConsole.class);
        game = new Game(
                new ConsolePlayer("Player1", gameConsoleMock) ,
                new ConsolePlayer("Player2", gameConsoleMock),
                new Machine(),
                gameConsoleMock
        );
    }

    @Test
    public void game_play(){
        Mockito.when(gameConsoleMock.nextLine())
                .thenReturn("4")
                .thenReturn("co")
                .thenReturn("co")
                .thenReturn("ch")
                .thenReturn("ch")
                .thenReturn("co")
                .thenReturn("ch")
                .thenReturn("ch")
                .thenReturn("co");
        game.play();
        assertEquals(4, game.player1.getScore());
        assertEquals(4, game.player2.getScore());
    }

    @Test
    public void game_show_score()
    {
        Mockito.when(gameConsoleMock.nextLine())
                .thenReturn("4")
                .thenReturn("co")
                .thenReturn("co")
                .thenReturn("ch")
                .thenReturn("ch")
                .thenReturn("co")
                .thenReturn("ch")
                .thenReturn("ch")
                .thenReturn("co");
        game.play();
        assertEquals("Player1 points : 4, Player2 points : 4", game.showScore());
    }

    @Test
    public void game_stop_scenario2()
    {
        Mockito.when(gameConsoleMock.nextLine())
                .thenReturn("1")
                .thenReturn("co")
                .thenReturn("co");
        game.play();
        assertEquals("Player1 points : 2, Player2 points : 2", game.showScore());
    }

    @Test
    public void should_print_to_the_console_times(){
        Mockito.when(gameConsoleMock.nextLine())
                .thenReturn("1")
                .thenReturn("co")
                .thenReturn("co");
        game.play();
        Mockito.verify(gameConsoleMock, Mockito.times(1)).printToConsole("Input number of moves");
        Mockito.verify(gameConsoleMock, Mockito.times(1)).printToConsole("Player1 points : 2, Player2 points : 2");
    }

    @Test
    public void should_play_when_player1_always_cooperate_player2_always_cheat(){
        game = new Game(
                new CooperatePlayer("Player1") ,
                new CheatPlayer("Player2"),
                new Machine(),
                gameConsoleMock
        );
        Mockito.when(gameConsoleMock.nextLine())
                .thenReturn("5");
        game.play();
        Mockito.verify(gameConsoleMock, Mockito.times(1)).printToConsole("Input number of moves");
        Mockito.verify(gameConsoleMock, Mockito.times(1)).printToConsole("Player1 points : -5, Player2 points : 15");
    }

    @Test
    public void play_for_one_copycat_player_and_one_cheat_player(){
        Mockito.when(gameConsoleMock.nextLine()).thenReturn("3");

        CopyCatPlayer copyCatPlayer = new CopyCatPlayer("Player2");

        game = new Game(
                new CheatPlayer("Player1") ,
                copyCatPlayer,
                new Machine(),
                gameConsoleMock
        );
        game.addPropertyChangeListener(copyCatPlayer);

        game.play();
        Mockito.verify(gameConsoleMock, Mockito.times(1)).printToConsole("Input number of moves");
        Mockito.verify(gameConsoleMock, Mockito.times(1)).printToConsole("Player1 points : 3, Player2 points : -1");
    }

    @Test
    public void play_for_one_copycat_player_and_one_console_player(){
        Mockito.when(gameConsoleMock.nextLine())
            .thenReturn("3")
            .thenReturn("ch")
            .thenReturn("co")
            .thenReturn("co");

        CopyCatPlayer copyCatPlayer = new CopyCatPlayer("Player2");
        game = new Game(
                new ConsolePlayer("Player1", gameConsoleMock) ,
                copyCatPlayer,
                new Machine(),
                gameConsoleMock
        );
        game.addPropertyChangeListener(copyCatPlayer);
        game.play();
        Mockito.verify(gameConsoleMock, Mockito.times(1)).printToConsole("Input number of moves");
        Mockito.verify(gameConsoleMock, Mockito.times(1)).printToConsole("Player1 points : 4, Player2 points : 4");
    }
}
