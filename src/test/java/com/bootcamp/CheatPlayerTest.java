package  com.bootcamp;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheatPlayerTest {

    @Test
    public void should_always_cheat(){
        Player player = new CheatPlayer("player1");
        assertEquals("ch", player.makeAMove());
    }
}
