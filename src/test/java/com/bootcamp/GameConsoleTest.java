package com.bootcamp;

import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class GameConsoleTest {

    @Test
    public void should_take_hello_input_from_scanner(){
        GameConsole console = new GameConsole(new Scanner("hello"));
        String line = console.nextLine();
        Assert.assertEquals("hello", line);
    }

    @Test
    public void should_take_greet_input_from_scanner(){
        GameConsole console = new GameConsole(new Scanner("greet"));
        String line = console.nextLine();
        Assert.assertEquals("greet", line);
    }
}
